import 'package:flutter/material.dart';
import 'package:tugas2_mobpro_kelompokmelinda/home_page.dart';

class Welcome extends StatefulWidget {
  static String tag = 'Welcome-Page';
  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  @override
  Widget build(BuildContext context) {
    final profil = Hero(
      tag: 'Profil',
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CircleAvatar(
          radius: 72.0,
          backgroundColor: Colors.transparent,
          backgroundImage: AssetImage('assets/foto2.jpg'),
        ),
      ),
    );
    final selamatDatang = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Selamat Datang Melinda',
        style: TextStyle(fontSize: 28.0, color: Colors.cyan),
      ),
    );
    final lanjutButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.lightBlueAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            Navigator.of(context).pushNamed(HomePage.tag);
          },
          color: Colors.lightBlueAccent,
          child: Text('Lanjutkan', style: TextStyle(color: Colors.white)),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(80.0),
          ),
          padding: const EdgeInsets.all(0.0),
        ),
      ),
    );
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[profil, selamatDatang, lanjutButton],
        ),
      ),
    );
  }
}
