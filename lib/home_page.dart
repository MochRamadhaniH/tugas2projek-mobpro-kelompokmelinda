import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class HomePage extends StatelessWidget {
  static String tag = 'home-page';

  @override
  Widget build(BuildContext context) {
    final foto = Hero(
      tag: 'Toga',
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CircleAvatar(
          radius: 72.0,
          backgroundColor: Colors.transparent,
          backgroundImage: AssetImage('assets/profile.jpg'),
        ),
      ),
    );
    final selamatDatang = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Halo Melinda',
        style: TextStyle(fontSize: 28.0, color: Colors.cyan),
      ),
    );
    final biodata = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Halo saya Melinda Fitria Yanuari, Berikut sedikit biodata dari saya. Umur saya 20 Tahun Mahasiswa Teknik Informatika Semester 5 di Sekolah Teknologi Tinggi Bandung, Alamat saya di kopo Margahayu, Hobi saya jalan jalan',
        style: TextStyle(fontSize: 16.0, color: Colors.white),
      ),
    );
    final isi = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Saya Ketua Kelompok dari tugas Mobile Programming yang Beranggotakan 4 orang dengan saya yaitu : Moch Ramadhani, Sugiman, dan Wildan. Sekian Terimakasih :)',
        style: TextStyle(fontSize: 16.0, color: Colors.white),
      ),
    );

    final body = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(28.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
          Colors.blue,
          Colors.lightBlueAccent,
        ]),
      ),
      child: Column(
        children: <Widget>[foto, selamatDatang, biodata, isi],
      ),
    );

    return Scaffold(
      body: body,
    );
  }
}
